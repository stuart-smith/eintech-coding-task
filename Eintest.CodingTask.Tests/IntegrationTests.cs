﻿using Eintech.CodingTask.Api.Services;
using Eintech.CodingTask.Core;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Eintest.CodingTask.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        public IntegrationTests()
        {
            var factory = new WebApplicationFactory<Program>();
            Client = factory.CreateDefaultClient();
        }

        public IRepositoryService? RepositoryService { get; }
        public HttpClient Client { get; }

        [TestMethod]
        public async Task CanGetPeopleWithApi()
        {
            var response = await Client.GetAsync("People");
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod]
        public async Task CanCreatePeopleWithApi()
        {
            var createPersonResponse = await Client.PostAsync($"People?name=Test Person at {DateTime.Now:s}", null);
            var createPersonResponseJson = await createPersonResponse.Content.ReadAsStringAsync();
            var allPeopleResponse = await Client.GetAsync("People");
            var allPeopleResponseJson = await allPeopleResponse.Content.ReadAsStringAsync();
            var person = JsonConvert.DeserializeObject<Person>(createPersonResponseJson);
            var allPeople = JsonConvert.DeserializeObject<IEnumerable<Person>>(allPeopleResponseJson);
            Assert.IsTrue(allPeople.SingleOrDefault(e=>e.Id == person.Id) != null);
        }

        [TestMethod]
        public async Task CanDeletePeopleWithApi()
        {
            var createPersonResponse = await Client.PostAsync($"People?name=Test Person at {DateTime.Now:s}", null);
            var createPersonResponseJson = await createPersonResponse.Content.ReadAsStringAsync();
            var allPeopleResponse = await Client.GetAsync("People");
            var allPeopleResponseJson = await allPeopleResponse.Content.ReadAsStringAsync();
            var person = JsonConvert.DeserializeObject<Person>(createPersonResponseJson);
            var allPeople = JsonConvert.DeserializeObject<IEnumerable<Person>>(allPeopleResponseJson);
            Assert.IsTrue(allPeople.SingleOrDefault(e => e.Id == person.Id) != null);

            await Client.DeleteAsync($"People?id={person.Id}");

            allPeopleResponse = await Client.GetAsync("People");
            allPeopleResponseJson = await allPeopleResponse.Content.ReadAsStringAsync();
            allPeople = JsonConvert.DeserializeObject<IEnumerable<Person>>(allPeopleResponseJson);
            Assert.IsFalse(allPeople.SingleOrDefault(e => e.Id == person.Id) != null);
        }
    }
}
