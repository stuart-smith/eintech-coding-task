using Eintech.CodingTask.Api.Services;
using Eintech.CodingTask.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Eintest.CodingTask.Tests
{
    [TestClass]
    public class UnitTests
    {
        public UnitTests()
        {
            Startup.Initialize();
            RepositoryService = Startup.HostApp.Services.GetService<IRepositoryService>();
        }

        public IRepositoryService? RepositoryService { get; }

        [TestMethod]
        public async Task CanCreatePerson()
        {
            var person = await RepositoryService.Create(new Person
            {
                Name = $"Test Person at {DateTime.Now:s}"
            });

            var people = await RepositoryService.GetAll<Person>();
            Assert.IsNotNull(people.Single(e=>e.Id == person.Id));
        }

        [TestMethod]
        public async Task CanGetPeople()
        {
            await RepositoryService.Create(new Person
            {
                Name = $"Test Person at {DateTime.Now:s}"
            });
            var people = await RepositoryService.GetAll<Person>();
            

            Assert.IsTrue(people.Any());
        }

        [TestMethod]
        public async Task CanDeletePeople()
        {
            var person = await RepositoryService.Create(new Person
            {
                Name = $"Test Person at {DateTime.Now.ToString("s")}"
            });
            var people = await RepositoryService.GetAll<Person>();
            Assert.IsTrue(people.Any(e => e.Id == person.Id));
            await RepositoryService.Delete<Person>(person.Id);
            people = await RepositoryService.GetAll<Person>();
            Assert.IsTrue(!people.Any(e => e.Id == person.Id));
        }


    }
}