﻿using Eintech.CodingTask.Api.Services;
using Eintech.CodingTask.Core.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Eintest.CodingTask.Tests
{
    public static class Startup
    {
        public static IHost HostApp { get; private set; }

        public static void Initialize()
        {
            HostApp = Host.CreateDefaultBuilder()
                .ConfigureServices((services) =>
                {
                    services.AddDbContext<EintechDbContext>(options =>
                    {
                        options.UseInMemoryDatabase(nameof(EintechDbContext));
                    });
                    services.AddTransient<IRepositoryService, RepositoryService>();
                })
                .Build();
        }
    }
}
