dotnet publish ./Eintech.CodingTask.sln -c Release
Start-Process -FilePath "dotnet" -ArgumentList @("Eintech.CodingTask.Api.dll", "urls=https://localhost:7181") -WorkingDirectory ".\Eintech.CodingTask.Api\bin\Release\net6.0"
cd .\codingtask-client
npm install
ionic serve --port 8103
