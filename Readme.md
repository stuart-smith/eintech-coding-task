# Eintech Coding Task

## Overview

This solution demonstrates how to add and remove people from a list.

The solution contains 3 projects:

- Eintech.CodingTask.Api - .NET 6.0/ASP .NET Core Web API project exposing API for adding and removing people from a table
- codingTask-client - Ionic/Vue3 front end that provides the user with the ability to do add and remove users from a list
- Eintech.CodingTask.Tests - .NET 6.0/MSTest project with some basic unit and integration tests covering the data access and API

## System Requirements

You must have the following tools installed to run the solution

- .NET 6.0 SDK
- .nodejs, npm, Vue CLI, Ionic

Note: To install Ionic you can use ```npm install -g @ionic/cli``` once npm is installed.

## How to run

You can run the solution by opening by running Run.ps1 in the root folder of the repository and then browsing to the following URL in your web browser: 

Run.ps1 will perform the following tasks:

- Release build of Eintech.CodingTask.Api project
- Run the Eintech.CodingTask.Api application
- Runs npm install
- Starts codingtask-client Vue app

## Notes

- To delete a person, swipe left on them to reveal the remove button
- Database will automatically be created when the API is run via Migrate method. For demo purposes, the solution uses In Memory database provider with EF Core.
- API is running at https://localhost:7181
- API documentation can be viewed at https://localhost:7181/swagger
- The Vue app can be accessed at http://localhost:8103
- Ionic was used for Vue project to save time as it has a good UI library out of the box