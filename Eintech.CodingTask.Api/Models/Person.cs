﻿using System.ComponentModel.DataAnnotations;

namespace Eintech.CodingTask.Core
{
    public class Person : Entity
    {
        /// <summary>
        /// The Name of the Person
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}