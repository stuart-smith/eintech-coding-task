﻿namespace Eintech.CodingTask.Core
{
    public interface IEntity
    {
        /// <summary>
        /// The Unique ID of the Entity
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The Date/Time when the entity was created
        /// </summary>
        public DateTime Created { get; set; }
    }

    public abstract class Entity : IEntity
    {
        /// <summary>
        /// The Unique ID of the Entity
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The Date/Time when the entity was created
        /// </summary>
        public DateTime Created { get; set; }
    }
}