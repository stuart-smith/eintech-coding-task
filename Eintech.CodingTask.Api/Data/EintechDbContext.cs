﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Eintech.CodingTask.Core.Data
{
    public class EintechDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }

        public EintechDbContext(DbContextOptions<EintechDbContext> options) : base(options) { }

        public override EntityEntry<TEntity> Add<TEntity>(TEntity entity)
        {
            if (entity is IEntity ent)
            {
                ent.Created = DateTime.UtcNow;
                return base.Add((TEntity)ent);
            }
            return base.Add(entity);
        }

    }
}
