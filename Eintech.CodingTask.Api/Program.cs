using Eintech.CodingTask.Core.Data;
using Microsoft.EntityFrameworkCore;
using Eintech.CodingTask.Api.Services;
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddDbContext<EintechDbContext>(options =>
{
    options.UseInMemoryDatabase(nameof(EintechDbContext));
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();
builder.Services.AddCors();
builder.Services.AddSwaggerDocument(config =>
{
    config.PostProcess = document =>
    {
        document.Info.Version = "v1";
        document.Info.Title = "Eintech Coding Task API";
        document.Info.Description = "A simple API demonstrate how to create and delete people from a list";

        document.Info.Contact = new NSwag.OpenApiContact
        {
            Name = "Stuart Smith",
            Email = "stuartsmithuk@hotmail.co.uk",
        };
    };
});

builder.Services.AddTransient<IRepositoryService, RepositoryService>();
builder.Services.AddTransient<IClientService, ClientService>();

var app = builder.Build();

app.UseOpenApi();
app.UseSwaggerUi3();


app.UseHttpsRedirection();

app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowAnyOrigin());

app.UseAuthorization();

app.MapControllers();

app.Run();
public partial class Program { }