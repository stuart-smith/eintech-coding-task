﻿using Eintech.CodingTask.Core;
using Eintech.CodingTask.Core.Data;
using Microsoft.EntityFrameworkCore;

namespace Eintech.CodingTask.Api.Services
{
    public class RepositoryService : IRepositoryService
    {
        public RepositoryService(EintechDbContext databaseContext)
        {
            DatabaseContext = databaseContext;
        }

        public EintechDbContext DatabaseContext { get; }

        public async Task<IEnumerable<T>> GetAll<T>() where T : Entity => await DatabaseContext.Set<T>().AsNoTracking().ToListAsync();

        public async Task<T> Create<T>(T entity) where T : Entity
        {
            DatabaseContext.Add(entity);
            await DatabaseContext.SaveChangesAsync();
            return entity;
        }

        public async Task Delete<T>(Guid id) where T : Entity
        {
            var entity = await DatabaseContext.FindAsync<T>(id);
            if (entity != null)
            {
                DatabaseContext.Remove(entity);
                await DatabaseContext.SaveChangesAsync();
            }
        }
    }
}
