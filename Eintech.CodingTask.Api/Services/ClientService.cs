﻿using NJsonSchema.CodeGeneration.TypeScript;
using NSwag;
using NSwag.CodeGeneration.OperationNameGenerators;
using NSwag.CodeGeneration.TypeScript;

namespace Eintech.CodingTask.Api.Services
{
    public interface IClientService
    {
        Task<string> GenerateTypeScriptClient(string url);
    }

    public class ClientService : IClientService
    {
        public async Task<string> GenerateTypeScriptClient(string url)
        {
            return await GenerateClient(
                document: await OpenApiDocument.FromUrlAsync(url),
                generateCode: (OpenApiDocument document) =>
                {
                    var settings = new TypeScriptClientGeneratorSettings();
                    settings.TypeScriptGeneratorSettings.TypeStyle = TypeScriptTypeStyle.Class;
                    settings.TypeScriptGeneratorSettings.TypeScriptVersion = 3.5M;
                    settings.TypeScriptGeneratorSettings.DateTimeType = TypeScriptDateTimeType.Date;
                    settings.Template = TypeScriptTemplate.Axios;
                    settings.PromiseType = PromiseType.Promise;
                    settings.HttpClass = HttpClass.HttpClient;
                    settings.WithCredentials = false;
                    settings.TypeScriptGeneratorSettings.NullValue = TypeScriptNullValue.Null;
                    settings.GenerateClientClasses = true;
                    settings.TypeScriptGeneratorSettings.ExportTypes = true;
                    settings.ExceptionClass = "ApiException";
                    settings.GenerateResponseClasses = true;
                    settings.ResponseClass = "SwaggerResponse";
                    settings.GenerateDtoTypes = true;
                    settings.OperationNameGenerator = new SingleClientFromOperationIdOperationNameGenerator();
                    settings.TypeScriptGeneratorSettings.MarkOptionalProperties = true;
                    settings.TypeScriptGeneratorSettings.GenerateCloneMethod = false;
                    settings.TypeScriptGeneratorSettings.EnumStyle = TypeScriptEnumStyle.Enum;
                    settings.TypeScriptGeneratorSettings.UseLeafType = false;
                    settings.TypeScriptGeneratorSettings.GenerateDefaultValues = true;
                    settings.TypeScriptGeneratorSettings.HandleReferences = false;
                    settings.TypeScriptGeneratorSettings.GenerateConstructorInterface = true;
                    settings.TypeScriptGeneratorSettings.ConvertConstructorInterfaceData = false;
                    settings.ImportRequiredTypes = true;
                    settings.UseGetBaseUrlMethod = false;
                    settings.QueryNullValue = "";
                    settings.UseAbortSignal = false;
                    settings.TypeScriptGeneratorSettings.InlineNamedDictionaries = false;
                    settings.TypeScriptGeneratorSettings.InlineNamedAny = false;
                    settings.IncludeHttpContext = false;
                    var generator = new TypeScriptClientGenerator(document, settings);
                    var code = generator.GenerateFile();

                    return code;
                }
            );
        }

        private async Task<string> GenerateClient(OpenApiDocument document, Func<OpenApiDocument, string> generateCode)
        {
            var code = generateCode(document);
            return await Task.FromResult(code);
        }
    }
}
