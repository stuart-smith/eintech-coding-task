﻿using Eintech.CodingTask.Core;

namespace Eintech.CodingTask.Api.Services
{
    public interface IRepositoryService
    {
        Task<T> Create<T>(T entity) where T : Entity;
        Task Delete<T>(Guid id) where T : Entity;
        Task<IEnumerable<T>> GetAll<T>() where T : Entity;
    }
}