﻿using Eintech.CodingTask.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace Eintech.CodingTask.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ScriptsController : ControllerBase
    {
        public IHttpContextAccessor HttpContextAccessor { get; }
        public IClientService ClientService { get; }

        public ScriptsController(IHttpContextAccessor httpContextAccessor,
            IClientService clientService)
        {
            HttpContextAccessor = httpContextAccessor;
            ClientService = clientService;
        }

        [HttpGet]
        [Route("api.ts")]
        public async Task<IActionResult> GenerateTypeScriptClient()
        {
            string url = $"{HttpContextAccessor.HttpContext.Request.Scheme}://{HttpContextAccessor.HttpContext.Request.Host}{HttpContextAccessor.HttpContext.Request.PathBase}/swagger/v1/swagger.json";
            var code = await ClientService.GenerateTypeScriptClient(url);
            return File(Encoding.UTF8.GetBytes(code), "text/plain");
        }
    }
}