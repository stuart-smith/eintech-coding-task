using Eintech.CodingTask.Api.Services;
using Eintech.CodingTask.Core;
using Microsoft.AspNetCore.Mvc;
using NJsonSchema.CodeGeneration.TypeScript;
using NSwag;
using NSwag.Annotations;
using NSwag.CodeGeneration.OperationNameGenerators;
using NSwag.CodeGeneration.TypeScript;

namespace Eintech.CodingTask.Api.Controllers
{


    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {
        public PeopleController(IRepositoryService repositoryService)
        {
            RepositoryService = repositoryService;
        }

        public IRepositoryService RepositoryService { get; }

        [HttpGet]
        [OpenApiOperation(nameof(GetAllPeople), "Get All People", "Returns a list of all People")]
        [SwaggerResponse(200, typeof(IEnumerable<Person>), Description = "A list of people")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> GetAllPeople() => Ok(await RepositoryService.GetAll<Person>());

        [HttpPost]
        [OpenApiOperation(nameof(AddPerson), "Add Person", "Adds a Person to the list")]
        [SwaggerResponse(200, typeof(Person), Description = "Returns the created Person")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> AddPerson(string name) => Ok(await RepositoryService.Create(new Person { Name = name }));

        [HttpDelete]
        [OpenApiOperation(nameof(DeletePerson), "Delete Person", "Deletes a Person from the list by their ID")]
        [SwaggerResponse(200, typeof(void), Description = "The person has been deleted from the list")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> DeletePerson(Guid id)
        {
            await RepositoryService.Delete<Person>(id);
            return Ok();
        }
    }
}